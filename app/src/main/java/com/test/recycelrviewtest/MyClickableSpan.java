package com.test.recycelrviewtest;

import android.content.Context;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Toast;

class MyClickableSpan extends ClickableSpan {

    String clicked;
    Context context;

    public MyClickableSpan(Context context, String string) {

        super();
        this.context = context;
        clicked = string;
    }

    public void onClick(View tv) {
        Toast.makeText(context.getApplicationContext(), ""+clicked, Toast.LENGTH_LONG).show();

    }

    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setUnderlineText(false); // set to false to remove underline
    }

}